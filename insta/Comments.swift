//
//  Comments.swift
//  insta
//
//  Created by sukh on 2018-03-29.
//  Copyright © 2018 sukh. All rights reserved.
//

import Foundation
import RealmSwift

class Comments : Object
{
    @objc dynamic var text : String = ""
    @objc dynamic var date : String = ""
    //@objc dynamic var owner: User? = nil
    //one to one  // inverse relationship
    //var owner = LinkingObjects(fromType: User.self, property: "comments")
    var photo = LinkingObjects(fromType: Photos.self, property: "comments")
}
