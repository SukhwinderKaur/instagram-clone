//
//  Photos.swift
//  insta
//
//  Created by sukh on 2018-03-29.
//  Copyright © 2018 sukh. All rights reserved.
//

import Foundation
import RealmSwift

class Photos : Object
{
    @objc dynamic var path : NSData?
    @objc dynamic var photoID = 0
    
    @objc dynamic var location : String = ""
    
    let comments = List<Comments>()
    
    var user = LinkingObjects(fromType: User.self, property: "photos")
    
    override static func primaryKey() -> String? {
        return "photoID"
    }
    
}
