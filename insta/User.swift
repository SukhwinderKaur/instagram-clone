//
//  User.swift
//  insta
//
//  Created by sukh on 2018-03-29.
//  Copyright © 2018 sukh. All rights reserved.
//

import Foundation
import RealmSwift

class User : Object
{
    @objc dynamic var name : String = ""
    @objc dynamic var password : String = ""
    @objc dynamic var status : String = ""
    var photos = List<Photos>()
}
