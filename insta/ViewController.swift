//
//  ViewController.swift
//  insta
//
//  Created by sukh on 2018-03-24.
//  Copyright © 2018 sukh. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {
    
    var user : User!
    @IBOutlet weak var uname: UITextField!
    @IBOutlet weak var passwd: UITextField!
    
    // -- MARK: database /  realm variables
    private var realm : Realm!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        // Realm init
        do {
            realm = try Realm()
        } catch {
            print("\(error)")
        }
        
    }
    
    
    @IBAction func loginClick(_ sender: UIButton) {
        let name = uname.text!
        //print(name)
        let pass = passwd.text!
       // print(pass)
        if name.isEmpty == true {
            print("username is empty")
            return
        }
        
        if pass.isEmpty == true {
            print("password is empty")
            return
        }
        
        
        
        // check if a user with this username already exists in the db
        let predicate = NSPredicate(format:"name = %@", name)
        let existingUser = realm.objects(User.self).filter(predicate)
        if (existingUser.count > 0 ) {
            print("found existing user")
            print(existingUser[0].password)
            
            user = existingUser[0]
           // print(user)
            
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // you need to cast this next line to the type of VC.
            let vc = storyboard.instantiateViewController(withIdentifier: "profile") as! profile
            // vc is the controller. Just put the properties in it.
            vc.name = uname.text!
            vc.desc = "i love cats"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else {
            print("no user found ")
            
        }
        
       // print("_______BYE!")
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

