//
//  photoViewController.swift
//  insta
//
//  Created by sukh on 2018-03-25.
//  Copyright © 2018 sukh. All rights reserved.
//

import UIKit
import RealmSwift
import CoreLocation

class photoViewController: UIViewController,UIImagePickerControllerDelegate,
UINavigationControllerDelegate, CLLocationManagerDelegate {
    let locationManager = CLLocationManager()
    
    let realm = try! Realm()
    @IBOutlet weak var pickedImage: UIImageView!
    
    @IBAction func cameraClick(_ sender: UIButton) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    
    
    @IBAction func galleryClick(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // save selected photo
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let realm = try! Realm()
        var loc : String = ""
        
        //location getting
        locationManager.requestAlwaysAuthorization()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.startUpdatingLocation()
        
        
        let location : CLLocation! = self.locationManager.location
        if(location == nil)
        {
            print("error")
            return
        }
        let latitude: Double = location!.coordinate.latitude
        let longitude: Double = location!.coordinate.longitude
        
        print("current latitude :: \(latitude)")
        print("current longitude :: \(longitude)")
        
        CLGeocoder().reverseGeocodeLocation(location!, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil)
            {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if placemarks!.count > 0
            {
                let pm = placemarks![0] as CLPlacemark
                
                loc = pm.subLocality!
                print(pm.subLocality!)
                //self.displayLocationInfo(placemark: pm)
            }
            else
            {
                print("Problem with the data received from geocoder")
            }
        })
        
        
        //image saving
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let newPhoto = Photos()
        newPhoto.path = UIImageJPEGRepresentation(selectedImage,0.0) as NSData?
        newPhoto.photoID = incrementID()
        newPhoto.location = loc
        let user = realm.objects(User.self).filter("name = %@", "sukh").first!
        
        // realm write
        do {
            try realm.write {
                user.photos.append(newPhoto)
                realm.add(user)
                realm.add(newPhoto)
                print("new Photo \(String(describing: newPhoto.path)) Added")
            }
        } catch {
            print("\(error)")
        }
        picker.dismiss(animated: true, completion: nil)
        
        pickedImage.image = selectedImage
        pickedImage.contentMode = .scaleAspectFit
        let alert = UIAlertController(title: "Wow", message: "Your image has been saved to Photo Library!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    // Generate auto-increment id manually
    func incrementID() -> Int {
        return (realm.objects(Photos.self).max(ofProperty: "photoID") as Int? ?? 0) + 1
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    
    @IBAction func saveData(_ sender: UIBarButtonItem) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // you need to cast this next line to the type of VC.
        let vc = storyboard.instantiateViewController(withIdentifier: "profile") as! profile
        // vc is the controller. Just put the properties in it.
        vc.name = "sukhkaur"
        vc.desc = "i love cats"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
