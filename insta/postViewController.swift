//
//  postViewController.swift
//  insta
//
//  Created by sukh on 2018-03-31.
//  Copyright © 2018 sukh. All rights reserved.
//

import UIKit
import RealmSwift

class postViewController: UIViewController {
    //var selctedPhoto : Photos!
    var imageid : Int?
    var realm: Realm!
    let session : URLSession = .shared
    @IBOutlet weak var postedImage: UIImageView!
    
    @IBOutlet weak var comment: UITextField!
    
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var allComments: UITextView!
    
    var newImage : UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
            realm = try Realm()
        } catch {
            print("\(error)")
        }
        
        print(imageid!)
        let request = realm.objects(Comments.self).filter("ANY photo.photoID = %@",imageid as! Int)
        print(request)
        for u in request
        {
            allComments.insertText("\n \(u.text) , \(u.date) \n \(u.text)")
        }
        postedImage.sizeToFit()
        postedImage.image = newImage
        
        let data = realm.object(ofType: Photos.self, forPrimaryKey: imageid)
        print(data!)
        location.text = data?.location
        print(data?.location)
        //loadComments(val : imageid!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func postComment(_ sender: UIButton) {
        realm = try! Realm()
        let photo = realm.object(ofType: Photos.self, forPrimaryKey: imageid)
        let comm = Comments()
        let txt = comment.text!
        comm.text = txt
        allComments.insertText(txt)
        //date
        let dateformatter = DateFormatter()
        
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let now = dateformatter.string(from: NSDate() as Date)
        comm.date = now
        
        allComments.insertText("\n \(txt) , \(now) \n")
        
        print(comm)
        // realm write
        do {
            try realm.write {
                print("Comment Added")
                photo?.comments.append(comm)
                realm.add(photo!)
                realm.add(comm)
                
            }
        } catch {
            print("error")
        }
        
    }
    
    func loadComments(val : Int) {
        
        /*let url = URL(string: "https://jsonplaceholder.typicode.com/comments")
        let task = session.dataTask(with:url!)
        {
            (data , response , error) in
            
            if(error != nil)
            {
                print(error!.localizedDescription)
                return
            }
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            //print(json!)
            
            //parsing json
            if let x = json as? [[String:Any]]
            {
                for index in 0...10 {
                   
                    let aObject = x[index]
                    print("--------------------")
                    let user = aObject["name"]
                    let date = aObject["email"]
                    let txt = aObject["body"]
                    print(user)
                     DispatchQueue.main.async {
                        
                        
                        // textView.inserText appends your string to the end
                        self.allComments.insertText("@\(String(describing: user!)): \t \(String(describing: date!)): \(String(describing: txt!)) \n \n")
                    }
                }
            }
            
        }
        
        task.resume()*/
        
     
       /*for u in request
       {
                let user = "sukh"
                let date = u.date
                let txt = u.text
                print(
                // textView.inserText appends your string to the end
                //self.allComments.insertText("@\(user) \t \(date) \(txt) \n")
        }*/
    }


}
