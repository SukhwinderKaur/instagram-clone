//
//  profile.swift
//  insta
//
//  Created by sukh on 2018-03-30.
//  Copyright © 2018 sukh. All rights reserved.
//

import UIKit
import RealmSwift


class profile : UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    
    var name : String!
    var desc : String!
    // MARK: - Properties
    
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    private var realm: Realm!
    private var photos: Results<Photos>!
    var token: NotificationToken!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        username.text = name
        status.text = desc
        /*let user = realm.objects(User.self).filter("name = %@", name).first
        if(user != nil)
        {
            username?.text = user?.name
            status?.text = user?.status
        }*/
        //setup collection View
        collectionView.delegate = self
        collectionView.dataSource = self
        let screen : CGRect = UIScreen.main.bounds
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: screen.width/4 , height: screen.height/5)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView!.collectionViewLayout = layout
        
        // Realm init
        do {
            realm = try Realm()
        } catch {
            print("\(error)")
        }
       
        photos = realm.objects(Photos.self)
        
        token = photos.observe({ (change) in
            self.collectionView?.reloadData()
        })
       // print(photos)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! photoCellCollectionViewCell
        // set saved image
        //let photos = Photos()
        //cell.backgroundColor = UIColor.red
        let res = realm.objects(Photos.self)
        let encode = res[indexPath.row].path! as Data
        //print(UIImage(data:encode))
        cell.myImage?.image = UIImage(data: encode)
        //print(cell.myImage)
        //print(cell)
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let cell = sender as? UICollectionViewCell,
            let indexPath = self.collectionView.indexPath(for: cell) {
        let res = realm.objects(Photos.self)
        let postView = segue.destination as! postViewController
        postView.newImage = UIImage(data : res[indexPath.row].path! as Data)
         postView.imageid = res[indexPath.row].photoID
        }
    }

}
