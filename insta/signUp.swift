//
//  signUp.swift
//  insta
//
//  Created by sukh on 2018-03-26.
//  Copyright © 2018 sukh. All rights reserved.
//

import UIKit
import RealmSwift
class signUp: UIViewController {
    private var realm: Realm!
   
    @IBOutlet weak var user_name: UITextField!
    
    
    @IBOutlet weak var pwd: UITextField!
    
    
    @IBOutlet weak var retype_pwd: UITextField!
    
    
    @IBAction func registerClick(_ sender: UIButton) {
        if user_name.text == "" {
            let alertController = UIAlertController(title: "Error", message: "Please enter user name", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
        }
        
        // get what the person wrote in the text box
        let username = user_name.text!
        let pass = pwd.text!
        let repass = retype_pwd.text!
        
        let user = User()
        var photo = Photos()
        if pass == repass
        {
            user.name = username
            user.password = pass
            user.status = "i love cats "
            user.photos = List()
            do  {
                try realm.write {
                    realm.add(user)
                    let alertController = UIAlertController(title: "Success", message: "Account Created", preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            catch {
                print("\(error)")
            }
        }
        else{
            let alertController = UIAlertController(title: "Error", message: "Password ~~Mismatch", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Realm init
        do {
            realm = try Realm()
        } catch {
            print("\(error)")
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
